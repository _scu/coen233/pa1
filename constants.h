//
//  constants.h
//  constants
//
//  Created by Mrithyunjay Jagannath Halinge on 11/17/19.
//  Copyright © 2019 Mrithyunjay Jagannath Halinge. All rights reserved.
//

#ifndef CONSTANTS_H
#define CONSTANTS_H

#define DEFAULT_PORT_NO 				8084

#define PACKET_IDENTIFIER 				0xFFFF

#define PACKET_TYPE_DATA 				0xFFF1
#define PACKET_TYPE_ACK 				0xFFF2
#define PACKET_TYPE_REJECT 				0xFFF3

#define REJECT_OUT_OF_SEQUENCE 			0xFFF4
#define REJECT_LENGTH_MISMATCH 			0xFFF5
#define REJECT_END_OF_PACKET_MISSING 	0xFFF6
#define REJECT_DUPLICATE 				0xFFF7

#endif /* CONSTANTS_H */