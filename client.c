//
//  client.c
//  client
//
//  Created by Mrithyunjay Jagannath Halinge on 11/17/19.
//  Copyright © 2019 Mrithyunjay Jagannath Halinge. All rights reserved.
//

#define TIMEOUT 3
#define RETRIES	3

#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

#include "constants.h"

int doClientOperation(
	uint8_t *message, uint8_t clientId, uint8_t segmentNo, int length,
	uint16_t packetIdentifier,
	int sockfd, struct sockaddr_in serv_add, socklen_t addr_size
) {
	int n, i, j;

	uint8_t payload[300];
	uint8_t buffer[300];

	uint16_t start, end, packetType, receivedPacketType, rejectSubCode;
	uint8_t receivedClientId, receivedSegmentNo;

	//add start of packet
	payload[0] = (uint8_t) (PACKET_IDENTIFIER >> 8);
	payload[1] = (uint8_t) (PACKET_IDENTIFIER & 0xFF);

	payload[2] = (uint8_t) clientId;
	
	payload[3] = (uint8_t) (PACKET_TYPE_DATA >> 8);
	payload[4] = (uint8_t) (PACKET_TYPE_DATA & 0xFF);
	payload[5] = (uint8_t) segmentNo;

	i = 7;
	j = 0;

	while(message[j] != '\0') {
		payload[i++] = message[j++];
	}

	payload[i] = '\0';

	payload[6] = (uint8_t) length;
	payload[262] = (uint8_t) (packetIdentifier >> 8);
	payload[263] = (uint8_t) (packetIdentifier & 0xFF);

	sendto(sockfd, (uint8_t *)payload, sizeof(payload), 
		0, (struct sockaddr *) &serv_add, addr_size);

	n = recvfrom(sockfd, (uint8_t *)buffer, sizeof(buffer), 
		0, (struct sockaddr *) &serv_add, &addr_size);

	buffer[n] = '\0';

	// verify start of packet
	if(n > 0) {
		start = ((uint16_t)buffer[0] << 8) | buffer[1];
		// if(start != PACKET_IDENTIFIER) {
		// 	printf("Start of packet error\n");
		// }
		receivedClientId = buffer[2];
		packetType = ((uint16_t)buffer[3] << 8) | buffer[4];

		if(packetType == PACKET_TYPE_ACK) {
			printf("ACK received\n");

			receivedSegmentNo = buffer[5];
			i = 6;
		} else if(packetType == PACKET_TYPE_REJECT) {
			receivedSegmentNo = buffer[7];
			rejectSubCode = ((uint16_t)buffer[5] << 8) | buffer[6];

			printf("Reject subcode: %x\n", rejectSubCode);

			if(rejectSubCode == REJECT_OUT_OF_SEQUENCE) {
				printf("Out of sequence packet recieved\n");
			} else if(rejectSubCode == REJECT_LENGTH_MISMATCH) {
				printf("Length mismatch\n");
			} else if(rejectSubCode == REJECT_END_OF_PACKET_MISSING) {
				printf("End of packet missing\n");
			} else if(rejectSubCode == REJECT_DUPLICATE) {
				printf("Duplicate packet received\n");
			}
			i = 8;
		}

		end = (uint16_t) buffer[i++] << 8;
		end = end | buffer[i];

		// if(end != PACKET_IDENTIFIER) {
		// 	printf("End of packet error\n");
		// }
		printf("Start: %x, clientId: %hhx, packet type: %x, seg: %hhx, end: %x\n\n", start, receivedClientId, packetType, receivedSegmentNo, end);

	}

	return n;
}

struct scenario {
	uint8_t segmentNo[5];
	int length;
	uint16_t packetIdentifier;
};

struct scenario scenarioSimulator(int scenario, int length) {
	struct scenario scene;
	switch(scenario) {
		case 1: // Out of sequence packets
			scene.segmentNo[0] = 1;
			scene.segmentNo[1] = 2;
			scene.segmentNo[2] = 4;
			scene.segmentNo[3] = 3;
			scene.segmentNo[4] = 5;
			scene.length = length;
			scene.packetIdentifier = PACKET_IDENTIFIER;
			break;

		case 2: // Duplicate packets
			scene.segmentNo[0] = 1;
			scene.segmentNo[1] = 2;
			scene.segmentNo[2] = 1;
			scene.segmentNo[3] = 3;
			scene.segmentNo[4] = 3;
			scene.length = length;
			scene.packetIdentifier = PACKET_IDENTIFIER;
			break;

		case 3: // Length Mismatch
			scene.segmentNo[0] = 1;
			scene.segmentNo[1] = 2;
			scene.segmentNo[2] = 3;
			scene.segmentNo[3] = 4;
			scene.segmentNo[4] = 5;
			scene.length = length + 42;
			scene.packetIdentifier = PACKET_IDENTIFIER;
			break;

		case 4: // End of packet missing
			scene.segmentNo[0] = 1;
			scene.segmentNo[1] = 2;
			scene.segmentNo[2] = 3;
			scene.segmentNo[3] = 4;
			scene.segmentNo[4] = 5;
			scene.length = length;
			scene.packetIdentifier = 0xFF;
			break;

		default:
			scene.segmentNo[0] = 1;
			scene.segmentNo[1] = 2;
			scene.segmentNo[2] = 3;
			scene.segmentNo[3] = 4;
			scene.segmentNo[4] = 5;
			scene.length = length;
			scene.packetIdentifier = PACKET_IDENTIFIER;
			break;
	}

	return scene;
	
}

int main(int argc, char *argv[]) { 
	int sockfd;
	struct sockaddr_in serv_add;

	socklen_t addr_size;

	int portno;

	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) { 
		printf("Socket creation failed");
		exit(EXIT_FAILURE);
	}

	if(argc < 2) {
		printf("No port provided. Using default port: %d\n", DEFAULT_PORT_NO);
		portno = DEFAULT_PORT_NO;
	} else {
		portno = atoi(argv[1]);
	}

	serv_add.sin_family = AF_INET;
	serv_add.sin_addr.s_addr = INADDR_ANY;
	serv_add.sin_port = htons(portno);

	addr_size = sizeof(serv_add);

	struct timeval tv;
	tv.tv_sec = TIMEOUT;
	tv.tv_usec = 0;
	setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(struct timeval));

	int counter, n, len;
	uint16_t packetIdentifier;

	struct scenario scene;

	uint8_t* message = (uint8_t*) "This is a test message!!";
	len = 0;
	while(message[len] != '\0') {
		len = len + 1;
	}

	uint8_t clientId;
	clientId = 0;

	for(;;) {
		printf("++++++ Pick a scenario ++++++\n");
		printf("1. Out of sequence error\n");
		printf("2. Duplicate Packet \n");
		printf("3. Length Mismatch\n");
		printf("4. End of packet error\n");
		printf("(Literally anything else* (*number)). Normal operation\n");
		int op;
		scanf("%d", &op);

		scene = scenarioSimulator(op, len);
		clientId++;

		for (int i = 0; i < 5; i++) {
			counter = 0;
			n = -1;
			while(n <= 0 && counter++ < RETRIES) {
				printf("Packet %d (Try %d)\n", i, counter);
				n = doClientOperation(message, clientId, scene.segmentNo[i], scene.length,
					scene.packetIdentifier,
					sockfd, serv_add, addr_size);
				if(n <= 0) {
					printf("No response from server for 3 seconds!\n");
					if(counter < RETRIES) {
						printf("Will try again\n");
						printf("Number of retries left: %d\n\n", (RETRIES - counter));
					}
				}
			}
			if(counter >= RETRIES) {
				printf("Server did not respond\n\n");
				exit(EXIT_FAILURE);
			}
		}
	}
	return 0;
}