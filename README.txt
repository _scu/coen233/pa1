===================================
COEN233: Computer Networks
Fall 2019

Programming Assignment 1

Name: 		Mrithyunjay Halinge
Student Id: W1552192
===================================

--------------------
1. Compilation
--------------------

Option 1:
Compile both server and client together:
`make`

Option 2:
Compile server and client separately:
`gcc -o server server.c`
`gcc -o client client.c`

--------------------
2. Start
--------------------

Starting server:
`make start_server`
or
`./server [portno]`

Starting client:
`make start_client`
or
`./client [portno]`

(portno is optional)

--------------------
3. Simulating Scenario
--------------------

Follow the steps on console to simulate the scenarios 


