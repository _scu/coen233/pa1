//
//  server.c
//  server
//
//  Created by Mrithyunjay Jagannath Halinge on 11/17/19.
//  Copyright © 2019 Mrithyunjay Jagannath Halinge. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

#include "constants.h"

void doServerOperation(
	int sockfd, struct sockaddr_in serv_add, socklen_t addr_size
) {
	int n, i, j, l;
	uint8_t buffer[300];
	uint8_t payload[300];
	uint8_t packet[300];
	struct sockaddr_storage serverStorage;

	int clientSegments[256] = {0};

	uint16_t start, end, packetType, receivedPacketType, rejectSubCode;
	uint8_t currentClientId, receivedSegmentNo, length;

	for(;;) {
		n = recvfrom(
			sockfd,
			(uint8_t *)buffer, sizeof(buffer),
			0, (struct sockaddr *) &serverStorage,
			&addr_size);
		buffer[n] = '\0';

		if(n > 0) {
			start = ((uint16_t)buffer[0] << 8) | buffer[1];
			currentClientId = buffer[2];
			receivedPacketType = ((uint16_t) buffer[3] << 8) | buffer[4];
			receivedSegmentNo = (uint8_t) buffer[5];
			length = (uint8_t) buffer[6];

			packetType = PACKET_TYPE_ACK;

			if(receivedSegmentNo <= clientSegments[currentClientId]) {
				packetType = PACKET_TYPE_REJECT;
				rejectSubCode = REJECT_DUPLICATE;
			} else if(receivedSegmentNo - clientSegments[currentClientId] != 1) {
				packetType = PACKET_TYPE_REJECT;
				rejectSubCode = REJECT_OUT_OF_SEQUENCE;
			}

			j = 7;
			i = 0;

			while(buffer[j] != '\0') {
				payload[i++] = buffer[j++];
			}

			if(i != length) {
				packetType = PACKET_TYPE_REJECT;
				rejectSubCode = REJECT_LENGTH_MISMATCH;
			}

			payload[i] = '\0';

			end = ((uint16_t)buffer[262] << 8) | buffer[263];

			if(end != PACKET_IDENTIFIER) {
				packetType = PACKET_TYPE_REJECT;
				rejectSubCode = REJECT_END_OF_PACKET_MISSING;
			}
			printf("Start: %x, clientId: %hhx, packet type: %x, seg: %hhx, length: %hhx, end: %x\n", start, currentClientId, packetType, receivedSegmentNo, length, end);
			printf("Payload: %s\n", payload);

			//add start of packet
			packet[0] = (uint8_t) (PACKET_IDENTIFIER >> 8);
			packet[1] = (uint8_t) (PACKET_IDENTIFIER & 0xFF);

			packet[2] = (uint8_t) currentClientId;

			packet[3] = (uint8_t) (packetType >> 8);
			packet[4] = (uint8_t) (packetType & 0xFF);

			j = 5;

			if(packetType == PACKET_TYPE_REJECT) {
				packet[j++] = (uint8_t) (rejectSubCode >> 8);
				packet[j++] = (uint8_t) (rejectSubCode & 0xFF);

				printf("Rejecting packet. Error: %x\n", rejectSubCode);
			} else {	
				clientSegments[currentClientId] = receivedSegmentNo;
				printf("Sending ACK\n");
			}

			packet[j++] = (uint8_t) receivedSegmentNo;

			packet[j++] = (uint8_t) (PACKET_IDENTIFIER >> 8);
			packet[j++] = (uint8_t) (PACKET_IDENTIFIER & 0xFF);

			packet[j++] = '\0';

			printf("Sending: ");

			int k = 0;

			while(packet[k] != '\0') {
				printf("%02x ", packet[k++]);
			}
			printf("\n");

			sendto(sockfd, (uint8_t *)packet, sizeof(packet),  
				0, (struct sockaddr *) &serverStorage, 
					addr_size);

			printf("\n");

		}
		
	}
}

int main(int argc, char *argv[]) { 
	int sockfd;
	struct sockaddr_in serv_add;
	socklen_t addr_size;

	int portno;

	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}

	if(argc < 2) {
		printf("No port provided. Using default port: %d\n", DEFAULT_PORT_NO);
		portno = DEFAULT_PORT_NO;
	} else {
		portno = atoi(argv[1]);
	}

	serv_add.sin_family = AF_INET;
	serv_add.sin_addr.s_addr = INADDR_ANY;
	serv_add.sin_port = htons(portno);


	addr_size = sizeof(serv_add);

	if (bind(sockfd, (const struct sockaddr *) &serv_add, addr_size) < 0 ) { 
		perror("bind failed");
		exit(EXIT_FAILURE);
	} 
	
	doServerOperation(sockfd, serv_add, addr_size);

	return 0;
} 